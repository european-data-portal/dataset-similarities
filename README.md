# Dataset Similarities

Detect potential similarities between datasets and record them.

## Installation for development

### Prerequisite

Install all of the following software:

 * Oracle / OpenJDK JDK >= 1.8
 * Apache Maven >= 3.2
 * GIT >= 1.9.4 
 
### Clone the sources
 
Run the following command to clone the repositories:

    git clone https://gitlab.com/european-data-portal/dataset-similarities.git
    
### Setup your system
Create or edit your Maven settings in ~/.m2/settings.xml. Insert the following XML:

    <settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0
       http://maven.apache.org/xsd/settings-1.0.0.xsd">
    
         <localRepository>${user.home}/.m2/repository</localRepository>
            <interactiveMode>true</interactiveMode>
            <usePluginRegistry>false</usePluginRegistry>
            <offline>false</offline>
        
            <profiles>
                <profile>
                    <id>dataset-similarities</id>
                    <properties>
                        <triplestore.url>URL</triplestore.url>
                        <datasetsimilarities.outputfile>similarities.csv</datasetsimilarities.outputfile>
                    </properties>
                </profile>
            </profiles>
        
            <activeProfiles>
                <activeProfile>dataset-similarities</activeProfile>
            </activeProfiles>
    </settings>
    
Load the mqa-service maven projects into your IDE (e.g. IntelliJ, Eclipse, Netbeans).   

### Execute the program ###

    mvn package exec:java

## Output ##
### DESCRITION ###
Per resource field similarity output.
Designed to be used flexibly for recombination. In DBs or in a map/reduce framework.

### VALUES ###
di = dataset ID
O:[di,x] = dataset ID + distribution ID
U:[di,x] = duplicate dataset ID + duplicate distribution ID

### KEYS ###
```
o = original
d = description duplicate  
t = title duplicate  
g = tags duplicate  
c = categories duplicate 
I = distribution Original, tuple set
U = distribution duplicate (URL, binary match), tuple set
s = similarity  
```

### Example per data field ###
```
{"o":"d1","d":"d12231","s":0.91}	// Descriptions
{"o":"d1","t":"d449911","s":0.92}	// title
{"o":"d1","g":"d138918","s":0.82} // tags
{"o":"d2","c":"d984","s":0.852}	// categories
{"I":["d128123","x213991"],"U":["d313","x99711"],"s":0.61}	// distributions
```
### USAGES ###
1. sorting
  + by "O", or simply alphanumerically
  + sort by similarity
	+ separate similarity files allow to easily sort by field type (column to visualize)
2. put into database with Document ID (O) to other table (D,..,U) maps
3. combine over "O" to get one single document with per field similarities
