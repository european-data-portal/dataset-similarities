package de.fhg.fokus.paneodp.similarities;

import java.util.HashSet;


/**
 * Created by Nils Rethmeier on 28.08.15.
 */
public class EdpDocument implements java.io.Serializable {
    public String docID;
    public String title;
    public String description;
    public HashSet<String> tags;
    public HashSet<String> categories;
    public String URLtail; // TODO currently unused

    public EdpDocument(String docID, String title, String description, HashSet<String> tags, HashSet<String> categories,
                       String URLtail) {
        /**
         * Class to represent a single document, struct style.
         */
        if (docID != "" && title != "" && description != "" && !tags.isEmpty() && !categories.isEmpty()) {
            this.docID = docID;
            this.title = title;
            this.description = removeExtraSpaces(description);
            this.tags = tags;
            this.categories = new HashSet<String>();
            // remove uri start stuff
            for (String url : categories) {
                this.categories.add(getEnding(url));
            }
            this.URLtail = URLtail;
        } else {
            print();
            throw new NullPointerException("one of the values is empty but should not be");
        }
    }

    public void print() {
        System.out.println("ID:" + this.docID + "\n"
                + "title:" + this.title + "\n"
                + "descr:" + this.description + "\n"
                + "tags:" + this.tags + "\n"
                + "categories:" + this.categories + "\n"
                + "URL:" + this.URLtail + "\n");
    }

    public String removeExtraSpaces(String org) {
        return org.replaceAll("\\r", "").replaceAll("\t", "").replaceAll("\\s+", " ").trim();
    }

    public String getEnding(String url) {
        return url.substring(url.lastIndexOf('/') + 1, url.length());
    }
}
