package de.fhg.fokus.paneodp.similarities;

import com.codesnippets4all.json.generators.JSONGenerator;
import com.codesnippets4all.json.generators.JsonGeneratorFactory;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;


/**
 * Created by nils rethmeier on 19.10.15.
 * edit 14.12.15
 */
public class DistributionDuplicateFinder {

    /**
     * Class that iterates over a sparql triplestoreGraph produces a list of matches for distribution URLs.
     * This is an abstraction of streamed, batch db requests/queries.
     * It a more compact version than the dataset approach in DataCollector.
     * Designed to run in a callable/thread via computeDistributionSimilarity()
     */
    // setup
    private String triplestoreUrl;
    private String triplestoreGraph;
    private String datasetPrefix;
    private String distributionPrefix;
    private int chunkSize = 7500;         // SPARQL LIMIT, or here: the line cache
    // states
    private int iteratorPosition = 0;//27974;   // SPARQL OFFSET for batch db queries
    private ResultSet results;
    private String currentDocID = "";
    private static final String ORIGINAL_DATASET = "I";
    private static final String DUPLICATE_DATASET = "U";
    private static final String SIMILARITY = "s";
    final static int maxDistributions = 1000;
    //    private QuerySolution prevQuerySolution = null;
    private Boolean start = true;

    public DistributionDuplicateFinder(String triplestoreUrl, String triplestoreGraph, int chunkSize) {
        /**
         * @param chunkSize is a caching value. 1000 is decently fast.
         */
        this.chunkSize = chunkSize;
        this.triplestoreUrl = triplestoreUrl;
        this.triplestoreGraph = triplestoreGraph;
    }

    public DistributionDuplicateFinder(String triplestoreUrl, String triplestoreGraph) {
        this(triplestoreUrl, triplestoreGraph, 8000);
    }

    public String getEnding(String url) { // taken from EDPdocument
        return url.substring(url.lastIndexOf('/') + 1, url.length());
    }

    private String updateQuery() {
        /**
         * Create a new sparql query, for batch stream processing.
         * @return the query string.
         */
        String endpointSparql = // TODO optimize this, but its may not be that necessary
                "select ?URL ?dataSet ?distribution\n" +
                        "where { ?dataSet <http://www.w3.org/ns/dcat#distribution> ?distribution. ?distribution ?field ?URL FILTER (regex (?field, \"accessURL\", \"i\") || regex (?field, \"downloadURL\", \"i\")).\n" +
                        "} offset " + Integer.toString(this.iteratorPosition) + // TODO used to have an orderby directive (10k lines limit), does this still work?
                        " limit " + Integer.toString(this.chunkSize);
        this.iteratorPosition += chunkSize; // prepare for calling next time, i.e. move one batch further
        return endpointSparql;
    }

    private void advanceBatch() {
        QueryExecution x = QueryExecutionFactory.sparqlService(triplestoreUrl, String.format(updateQuery(), triplestoreGraph));
        ResultSet resultz = x.execSelect();
        resultz.hasNext(); // this fails if end of database is reached
        this.results = resultz;
    }

    public String nextDoc() {
        /**
         * Return the next document. Returns null if no more documents are in the database.
         * @return: the next document (Resource URL, dataSet ID, distributionID) or null if end of DB
         */
        QuerySolution n = null;
        n = this.next();
        this.start = false; // so data retrieval only initializes pulling a batch at te start and not every time
        // in case there is more data
        if (n != null) {
            return String.format("%s\t%s\t%s",
                    n.get("URL").toString(),                // resource URL
                    getEnding(n.get("dataSet").toString()), // datasetID
                    getEnding(n.get("distribution").toString())); // distributionID
        } else { // longer but explicit else
            return null;
        }
    }

    @Deprecated // works nicely but unused
    public static String getLastPartOfURI(String uri) {
        URI last = null;
        try {
            last = new URI(uri);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        String path = last.getPath();
        return path.substring(path.lastIndexOf('/') + 1);
    }

    private QuerySolution next() {
        /**
         * make batch iteration feel like full database iteration. Returns null iif end of database is reached
         * @return the next line of db results, or null if last document form data base was returned in the step before.
         */
        if (this.start) { // this is a bit ugly
            advanceBatch();
        }
        if (this.results.hasNext()) {
            // a) if data is left in the batch, return it
            return this.results.next();
        } else {
            // b) otherwise get the next batch ...
            advanceBatch();
            // c) or handle if end of database is reached
            try {
                QuerySolution n = this.results.next();
                return n;
            } catch (NoSuchElementException nse) {
                return null;
            }
        }
    }

    public static void collectDistributionURLs(String ip, String db, String urlFile, int maxDistributions) throws IOException {
        int numDocs = 0;
        FileOutputStream fos    = null;
        OutputStreamWriter out  = null;
        long oldtime = System.nanoTime();
        DistributionDuplicateFinder distIter = new DistributionDuplicateFinder(ip, db);
        try {
            fos = new FileOutputStream(urlFile);
            out = new OutputStreamWriter(fos, "UTF-8");
            for (int i = 0; i != maxDistributions; i++) { // no while so limit can be set
                if (i%1000 == 0) { // see feedback
                    long nT = System.nanoTime(); // measure time
                    System.out.println(i + ": took time in ms =" + Long.toString((nT- oldtime)/1000000));
                    oldtime = nT;
                }
                String urls = distIter.nextDoc();
                if (urls != null) {
                    numDocs += 1;
                    out.write(urls);
                    out.write("\n");
                    continue;
                }
                break; // end of DB reached, we only get here if urls!=null.
            }
        } finally {
            if (fos != null) {
                out.flush(); // this first, cant flush a closed stream
                fos.close();
                out.close();
            }
        }
    }

    public static void appendDulicatesToMap(ArrayList<String[]> repeatedLines, HashMap<String, ArrayList<String[]>> DataSetDuplicates) {
        assert !repeatedLines.isEmpty() : "duplicates should not be empty";
        int len = repeatedLines.size();
        for (int org = 0; org!=len; org++) {
            for (int dup = 0; dup!=len; dup++){
                if (org!=dup) { // since a thing is not a duplicate of itself
                    String[] dupl = {repeatedLines.get(org)[2], repeatedLines.get(dup)[1], repeatedLines.get(dup)[2]};
                    // append to older findings
                    if (DataSetDuplicates.containsKey(repeatedLines.get(org)[1])) {
                        ArrayList<String[]> olderFindings = DataSetDuplicates.get(repeatedLines.get(org)[1]);
                        olderFindings.add(dupl);
                        DataSetDuplicates.put(repeatedLines.get(org)[1], olderFindings);
                    }
                    // New key, i.e. dataset for which duplicates do not yet exist
                    else {
                        ArrayList<String[]> findings = new ArrayList<String[]>(); findings.add(dupl);
                        DataSetDuplicates.put(repeatedLines.get(org)[1], findings);
                    }
                }

            }
        }
    }

    public static  HashMap<String, ArrayList<String[]>> mapURLDublicatesToDatasets(String distributionURLs) throws IOException {
        /**
         * Finds duplicates via repeating urls in adjacent lines. This assumes that the input is sorted by url.
         * Using unix sort.
         * $sort k-1 unsortedData > sortedByUrl
         * @return: a map that allows to query for each dataset, weather it links to a distribution which itself links to a resource/download url that
         * another distributions also links to. Allows transitive duplicate detection of datasets as in:
         * Duplicate datasets: DataSetX ==(sameAs) DataSetY via transitivity as follows.
         * DatasetY -> DistributionX -> URLtoResource == sameAs == <-> DistributionY <-> DatasetY
         * ==> DatasetX==DataSetY
         */
        FileInputStream fis    = null;
        BufferedReader in  = null; // for readLine
        HashMap<String, ArrayList<String[]>> urlsToDSets = new HashMap<String, ArrayList<String[]>>();
        String[] prevLine = {""};
        String currLine = null;
        ArrayList<String[]> urlDuplicates = new ArrayList<String[]>(); // Store info on found url duplicates
        Boolean inDuplicate = false;            // again with
        try {
            fis = new FileInputStream(distributionURLs);
            in = new BufferedReader(new InputStreamReader(fis, "UTF-8"));
            while ((currLine = in.readLine()) != null) {
                String[] cLine = currLine.split("\t");
                if (cLine[0].equals(prevLine[0])) { // if there is a duplicate, indicated by a repeating line
                    if (!inDuplicate) {
                        inDuplicate = true;
                        urlDuplicates.add(cLine);
                        urlDuplicates.add(prevLine);
                    } else {
                        urlDuplicates.add(cLine);
                    }
                } else { // no repeating line, 2 cases
                    if (inDuplicate) { // 1st case, just ended repeating urls, aka duplicates
                        inDuplicate = false;                        // prep for finding nex duplicate url lines
                        appendDulicatesToMap(urlDuplicates, urlsToDSets);
                        urlDuplicates = new ArrayList<String[]>();  // init for a new duplicate tuple
                    } else {           // 2nd case, no duplicates so just ignore
                        // nothing to do here, explicit else for readability
                        // common case is that nothing repeats, this else "fires" 99% of the time.
                    }
                }
                prevLine = cLine;

            }
        } finally {
            if (fis != null) {
                in.close();
                fis.close();
            }
        }
        return urlsToDSets;
    }

    public static void mapURLDublicatesToDatasetIDs(String distributionURLs) throws IOException {
        /**
         * Finds duplicates via repeating urls in adjacent lines. This assumes that the input is sorted by url.
         * Using unix sort.
         * $sort k-1 unsortedData > sortedByUrl
         * @return: a map that allows to query for each dataset, weather it links to a distribution which itself links to a resource/download url that
         * another distributions also links to. Allows transitive duplicate detection of datasets as in:
         * Duplicate datasets: DataSetX ==(sameAs) DataSetY via transitivity as follows.
         * DatasetY -> DistributionX -> URLtoResource == sameAs == <-> DistributionY <-> DatasetY
         * ==> DatasetX==DataSetY
         */
        // Input format (not commas):
        // Resource URL, dataSetId, distributionId

        FileInputStream fis    = null;
        BufferedReader in  = null; // for readLine
        FileOutputStream fos    = null;
        OutputStreamWriter out  = null;
        String[] prevLine = {""};
        String currLine = null;
        ArrayList<String[]> urlDuplicates = new ArrayList<String[]>(); // Store info on found url duplicates
        Boolean inDuplicate = false;            // again with
        try {
            // Where to store resulting similarities
            fos = new FileOutputStream(distributionURLs+"_distribution.sims");
            out = new OutputStreamWriter(fos, "UTF-8");
            fis = new FileInputStream(distributionURLs);
            in  = new BufferedReader(new InputStreamReader(fis, "UTF-8"));
            while ((currLine = in.readLine()) != null) {
                String[] cLine = currLine.split("\t");
                if (cLine[0].equals(prevLine[0])) { // if there is a duplicate, indicated by a repeating line
                    if (!inDuplicate) {
                        inDuplicate = true;
                        urlDuplicates.add(prevLine);
                        urlDuplicates.add(cLine);
                    } else {
                        urlDuplicates.add(cLine);
                    }
                } else { // no repeating line, 2 cases
                    if (inDuplicate) { // 1st case, just ended repeating urls, aka duplicates
                        inDuplicate = false;                        // prep for finding next duplicate url lines
                        // create duplicates {"Original":["datasetID","distributionID"],"Duplicate":["duplicateDatasetID","duplicateDistributionID"],"similarity":0.61}	// distributions
                        writeDuplicatesAsJSON(urlDuplicates, out);
                        urlDuplicates = new ArrayList<String[]>();  // init for a new duplicate tuple
                    } else {           // 2nd case, no duplicates so just ignore
                        // nothing to do here, explicit else for readability
                        // common case is that nothing repeats, this else "fires" 99% of the time.
                    }
                }
                prevLine = cLine;

            }
        } finally {
            if (fis != null) {
                in.close();
                fis.close();
                out.flush();
                fos.flush();
                out.close();
                fos.close();
            }
        }
    }

    public static void writeDuplicatesAsJSON(ArrayList<String[]> urlDuplicates, OutputStreamWriter out) throws IOException {
        /**
         * Transform data into JSON and store in file.
         */
        JSONGenerator jsonGen=JsonGeneratorFactory.getInstance().newJsonGenerator(); // one instance suffices
        for(String[] originalTriple : urlDuplicates) {
            for(String[] duplicateTriple : urlDuplicates){
                if(!duplicateTriple.equals(originalTriple)){
                    // transform data into json
                    String json = createJSONline(originalTriple, duplicateTriple, jsonGen);
//                    // store in file
                    out.write(json);
                    out.write("\n");
                }
            }
        }
    }

    public static String createJSONline(String[] originalTriple, String[] duplicateTriple, JSONGenerator generator){
        /**
         * Transform duplicates to JSON.
         */
        Map data = new HashMap();
        // String[0] is URL, its unused
        // Original [datasetID, distributionID]
        data.put(ORIGINAL_DATASET, new String[]{originalTriple[1], originalTriple[2]});
        data.put(DUPLICATE_DATASET, new String[]{duplicateTriple[1], duplicateTriple[2]});
        data.put(SIMILARITY, 1);

        // serialize to JSON
        return generator.generateJson(data);
    }

    public static void executeScriptInCallable(String unsorted, String sorted) throws FileNotFoundException, UnsupportedEncodingException {
        // sorting that works inside a Callable.
        // Version for non-main thread, see difference in executeScriptInMainThread.
        try { // 1. execute sort directly
            Process rt2 = Runtime.getRuntime().exec("sort -k1 " + unsorted + " -o " + sorted);
            rt2.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void executeScriptInMainThread(String unsorted, String sorted) throws FileNotFoundException, UnsupportedEncodingException {
        // sorting has to be in a script, otherwise java wont wait for it to complete, breaking progam logic
        // Version for main thread. See executeScriptInCallable for difference.
        // 1. create the script from java so its all contained in a .sh
        PrintWriter writer = new PrintWriter("sorter.sh", "UTF-8");
        writer.print("export LC_ALL=C\nsort -k1 -t$'\t' $1 -o $2"); // export for correct unicode handling
        writer.close();
        try { // 2. execute sort
            Process rt = Runtime.getRuntime().exec("sh sorter.sh " + unsorted + " " + sorted);
            rt.waitFor();
            // 3. erase tmp file
            Process rt2 = Runtime.getRuntime().exec("rm sorter.sh");
            rt2.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void handleStream(InputStream input) {
        // see http://stackoverflow.com/questions/10365402/java-process-invoked-by-processbuilder-sleeps-forever
            int c;
        try {
            while ((c = input.read()) != -1) {
                // nothing
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //******************************************************************************************************************
    //*************************************     MAIN     ***************************************************************
    //******************************************************************************************************************
    public static void computeDistributionSimilarity(String args[]) throws IOException, InterruptedException {
        /**
         * Takes server url, dataset name in DB, output file, outputfile for seorted in between step.
         * Returns a list form [datasetID, attached_distribution_1] to [duplicate_datasetID, duplicate_DistributionID]
         * Hence, main key value is:
         * datasetID
         */
        // download DB distributions
        final int SERVER_URL = 0;
        final int DATASET = 1;
        final int DATASET_SIMILARITIES_OUT = 2; // unused, documentation only
        final int MAX_DISTRIBUTIONS = Integer.parseInt(args[4]);
        final String OUTPUT_FILE_PATH = args[DATASET_SIMILARITIES_OUT] + "_unsorted";
        final String OUTPUT_FILE_PATH_SORTED = args[DATASET_SIMILARITIES_OUT] + "_sorted";
        DistributionDuplicateFinder ds = null;

        if (MAX_DISTRIBUTIONS > 0) {
            // collect data form Data Base
            collectDistributionURLs(args[SERVER_URL], args[DATASET], OUTPUT_FILE_PATH, MAX_DISTRIBUTIONS);
            // Preprocess the input, aka sort it
            executeScriptInCallable(OUTPUT_FILE_PATH, OUTPUT_FILE_PATH_SORTED);
            // assemble duplicates
            mapURLDublicatesToDatasetIDs(OUTPUT_FILE_PATH_SORTED);
            // cleanup temporary files
            Runtime.getRuntime().exec("rm " + OUTPUT_FILE_PATH_SORTED + " " + OUTPUT_FILE_PATH);
        }
    }

    //******************************************************************************************************************
    //*************************************     MAIN     ***************************************************************
    //******************************************************************************************************************
    @Deprecated
    public static HashMap<String, ArrayList<String[]>> computeDistributionSimilarityMap(String args[]) throws IOException, InterruptedException {
        /**
         * TODO erase if not used. FYI: Points to a method of combining all distribution similarities for one datasetID
         * NOTE: in Memory
         *
         * Takes server url, dataset name in DB, output file, outputfile for seorted in between step.
         * Returns a map form datasetID to (attached_distribution_1 (duplicate_datasetID, duplicate_DistributionID))
         *
         * Dataset to duplicates map: HashMap<String, ArrayList<String[]>>
         *
         * one
         * datasetID1 --- attached_distribution_1, (duplicate_datasetID, duplicate_DistributionID)) |
         *            --- attached_distribution_2, (duplicate_datasetID, duplicate_DistributionID)) |> = ArrayList<String[]>
         *            --- attached_distribution_3, (duplicate_datasetID, duplicate_DistributionID)) |
         *            ...
         *                |_______________________,_____________________,_________________________|
         *                                                  v
         *                                    String[] a = a[0], a[1], a[3]
         */
        // download DB distributions
        final int SERVER_URL = 0;
        final int DATASET = 1;
        final int DATASET_SIMILARITIES_OUT = 2; // unused, documentation only
        final int MAX_DISTRIBUTIONS = Integer.parseInt(args[4]);
        final String OUTPUT_FILE_PATH = args[DATASET_SIMILARITIES_OUT] + "_unsorted";
        final String OUTPUT_FILE_PATH_SORTED = args[DATASET_SIMILARITIES_OUT] + "_sorted";
        DistributionDuplicateFinder ds = null;

        // collect data form Data Base
        collectDistributionURLs(args[SERVER_URL], args[DATASET], OUTPUT_FILE_PATH, MAX_DISTRIBUTIONS);
        // Preprocess the input, aka sort it
        executeScriptInCallable(OUTPUT_FILE_PATH, OUTPUT_FILE_PATH_SORTED);
        // assemble duplicates in a file
        return mapURLDublicatesToDatasets(OUTPUT_FILE_PATH_SORTED);
    }
}