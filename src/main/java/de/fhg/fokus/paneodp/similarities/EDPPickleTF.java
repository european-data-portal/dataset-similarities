package de.fhg.fokus.paneodp.similarities;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by nils rethmeier on 14.10.15.
 */
public class EDPPickleTF implements Serializable {
    /* Members that store info necessary for TFIDF
     * Class very similar to {@link de.fhg.fokus.paneodp.similarities.EdpDocument()}
     **/
    public String docID = "";
    public HashMap<String, Integer> title = new HashMap<String, Integer>();
    public HashMap<String, Integer> description = new HashMap<String, Integer>();
    public HashSet<String> tags = new HashSet<String>();
    public HashSet<String> categories = new HashSet<String>();

//    // C'TOR
//    public EDPPickleTF(String docID, HashMap<String, Integer> title, HashMap<String, Integer> description, HashSet<String> tags, HashSet<String> categories) {
//        /**
//         * Class to represent a single document, struct style.
//         */
//        if (docID != "" && !title.isEmpty() && !description.isEmpty() && !tags.isEmpty() && !categories.isEmpty()) {
//            this.docID = docID;
//            this.title = title;
//            this.description = description;
//            this.tags = tags;
//            this.categories = new HashSet<String>();
//            // remove uri start stuff
//            for (String url : categories) {
//                this.categories.add(getEnding(url));
//            }
//        } else {
//            this.docID = "";
//            this.title = new HashMap<String, Integer>();
//            this.description = new HashMap<String, Integer>();
//            this.tags = new HashSet<String>();
//            this.categories = new HashSet<String>();
//            // remove uri start stuff
//            for (String url : categories) {
//                this.categories.add(getEnding(url));
//            }
//            // FIXME fixed // throw new NullPointerException("one of the values is empty but should not be");
//        }
//    }

    // C'TOR
    public EDPPickleTF(String docID, HashMap<String, Integer> title, HashMap<String, Integer> description, HashSet<String> tags, HashSet<String> categories) {
        /**
         * Class to represent a single document, struct style.
         */
        this.docID = docID;
        this.title = title;
        this.description = description;
        this.tags = tags;
        this.categories = new HashSet<String>();
        // remove uri start stuff
        for (String url : categories) {
            this.categories.add(getEnding(url));
        }
    }

    public void print() {
        System.out.println("ID:" + this.docID + "\n"
                + "title:" + this.title + "\n"
                + "descr:" + this.description + "\n"
                + "tags:" + this.tags + "\n"
                + "categories:" + this.categories + "\n");
    }

    public String removeExtraSpaces(String org) {
        return org.replaceAll("\\r", "").replaceAll("\t", "").replaceAll("\\s+", " ").trim();
    }

    public String getEnding(String url) {
        return url.substring(url.lastIndexOf('/') + 1, url.length());
    }
}
