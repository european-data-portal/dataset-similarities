package de.fhg.fokus.paneodp.similarities;

import com.codesnippets4all.json.generators.JSONGenerator;
import com.codesnippets4all.json.generators.JsonGeneratorFactory;
import edu.stanford.nlp.ling.Word;
import edu.stanford.nlp.process.PTBTokenizer;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tartarus.snowball.ext.englishStemmer;

import java.io.*;
import java.nio.file.Files;
import java.util.*;
import java.util.concurrent.*;

/**
 * Created by Nils Rethmeier on 24.08.15.
 */

public class DataCollector {
    public enum TEXT_FIELD {
        TITLE, Description
    }
    // dataset ids contants
    private static final String ORIGINAL_DATASET = "o";
    private static final String DUPLICATE_DESCRIPTION = "d";
    private static final String DUPLICATE_TITLE = "t";
    private static final String DUPLICATE_TAGS = "g";
    private static final String DUPLICATE_CATEGORIES = "c";
    private static final String SIMILARITY = "s";

    // sim thresholds
    private double titleThreshold = 0.8d;
    private double descriptionThreshold = 0.8d;
    private double categoryThreshold = 0.8d;
    private double tagThreshold = 0.8d;

    private static Logger LOG = LoggerFactory.getLogger(DataCollector.class);

    private String triplestoreUrl;
    private String triplestoreGraph;
    private String outputFile;
    private HashSet<String> stopwordsEnLong = null;
    private Boolean DEBUG = true;
    private englishStemmer stemmer = new englishStemmer();
    private PTBTokenizer<Word> tkzr = null;
    private HashMap<String, Integer> rawDescriptionDFs = null;
    private HashMap<String, Integer> rawTitleDFs = null;
    private int numDocs = 0;
    private int maxDocs = 100000000;
    private boolean USE_JACCARD_ONLY_ON_LARGER_SETS = false; // on small sets and in wrong/default data, the same tags are very common


    // C'Tors
    public DataCollector(String triplestoreUrl, String triplestoreGraph, String outputFilePath) {
        this(triplestoreUrl, triplestoreGraph, outputFilePath, 100000000);
    }

    public DataCollector(String triplestoreUrl, String triplestoreGraph, String outputFilePath, int maxDatasets) {
        this.triplestoreUrl = triplestoreUrl;
        this.triplestoreGraph = triplestoreGraph;
        this.outputFile = outputFilePath;
        System.out.println(this.outputFile);
        this.stopwordsEnLong = this.loadStopwords();
        this.maxDocs = maxDatasets;
    }

    public static String wordOnly(String word) {
        // remove symbols, and numbers
        return word.replaceAll("\\W", "").replaceAll("\\d", "");
    }

    //******************************************************************************************************************
    //*************************************     MAIN     ***************************************************************
    //******************************************************************************************************************
    public static void main(String args[]) throws ExecutionException, InterruptedException {
        final int SERVER_URL = 0;
        final int DATASET = 1;
        final int OUTPUT_FILE_PATH = 2;
        final int MAX_DATASETS = 3;
        final int MAX_DISTRIBUTIONS = 4;

        DataCollector dc = null;

        // compute distribution similarities - this is basically map/reduce(sort)/combine
        // return the result via anonymous callable
        System.out.println("run callable");
        final String[] argz = args;
        ExecutorService exec = Executors.newFixedThreadPool(1);
        Future<HashMap<String, ArrayList<String[]>>> dupl = exec.submit(new Callable<HashMap<String, ArrayList<String[]>>>() {
            public HashMap<String, ArrayList<String[]>> call() throws InterruptedException {
                HashMap<String, ArrayList<String[]>> duplicates = null;
                try {
                    DistributionDuplicateFinder.computeDistributionSimilarity(argz);
                } catch (IOException e) {
                    LOG.error("", e);
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    LOG.error("", e);
                    e.printStackTrace();
                }
                return duplicates; // unused
            }
        });
        exec.shutdown();

        // compute dataset similarities (TFIDF and Jaccard scoring of content)
        dc = new DataCollector(args[SERVER_URL], args[DATASET], args[OUTPUT_FILE_PATH], Integer.parseInt(args[MAX_DATASETS]));
        try {
            dc.findSimilarities();
        } catch (IOException e) {
            LOG.error("", e);
        } catch (ClassNotFoundException e) {
            LOG.error("", e);
        }
    }

    public void findSimilarities() throws IOException, ClassNotFoundException {
            String infoCorpus = Files.createTempFile("corpusTermFreqs", ".tmp").toAbsolutePath().toString();
//            deleteFile(infoCorpus);
            System.out.println("Generate TF");
            genTFbuildCorpus(infoCorpus);
            System.out.println("Generate TFIDF");
            String[] tfidfCorpora = transIntoTFIDFCorpus(infoCorpus);
            System.out.println("Compute Sims");
            LOG.info("###Similarities###");
            computeSimilarities(tfidfCorpora, 0.8);
    }

    //******************************************************************************************************************
    //************************************* init stuff *****************************************************************
    //******************************************************************************************************************
    private HashSet<String> loadStopwords() {
        HashSet<String> hs = new HashSet<String>();
        // these stop words are made compatible with an implementation of penn treebank word tokenizer.
        // English
        String[] sws = "a,able,about,above,abroad,according,accordingly,across,actually,adj,after,afterwards,again,against,ago,ahead,ai,all,allow,allows,almost,alone,along,alongside,already,also,although,always,am,amid,amidst,among,amongst,an,and,another,any,anybody,anyhow,anyone,anything,anyway,anyways,anywhere,apart,appear,appreciate,appropriate,are,around,as,aside,ask,asking,associated,at,available,away,awfully,back,backward,backwards,be,became,because,become,becomes,becoming,been,before,beforehand,begin,behind,being,believe,below,beside,besides,best,better,between,beyond,both,brief,but,by,c'mon,ca,came,can,cannot,cant,caption,cause,causes,certain,certainly,changes,clearly,co,co.,com,come,comes,concerning,consequently,consider,considering,contain,containing,contains,corresponding,could,course,currently,'d,dare,definitely,described,despite,did,different,directly,do,does,doing,done,down,downwards,during,each,edu,eg,eight,eighty,either,else,elsewhere,end,ending,enough,entirely,especially,et,etc,even,ever,evermore,every,everybody,everyone,everything,everywhere,ex,exactly,example,except,fairly,far,farther,few,fewer,fifth,first,five,followed,following,follows,for,forever,former,formerly,forth,forward,found,four,from,further,furthermore,get,gets,getting,given,gives,go,goes,going,gone,got,gotten,greetings,had,half,happens,hardly,has,have,having,he,hello,help,hence,her,here,hereafter,hereby,herein,hereupon,hers,herself,hi,him,himself,his,hither,hopefully,how,howbeit,however,hundred,i,ie,if,ignored,immediate,in,inasmuch,inc,inc.,indeed,indicate,indicated,indicates,inner,inside,insofar,instead,into,inward,is,it,its,itself,just,k,keep,keeps,kept,know,known,knows,last,lately,later,latter,latterly,least,less,lest,let,like,liked,likely,likewise,little,'ll,look,looking,looks,low,lower,ltd,'m,made,mainly,make,makes,many,may,maybe,me,mean,meantime,meanwhile,merely,might,mine,minus,miss,more,moreover,most,mostly,mr,mrs,much,must,my,myself,name,namely,nd,near,nearly,necessary,need,needs,neither,never,neverf,neverless,nevertheless,new,next,nine,ninety,no,nobody,non,none,nonetheless,noone,no-one,nor,normally,not,nothing,notwithstanding,novel,now,nowhere,n't,obviously,of,off,often,oh,ok,okay,old,on,once,one,ones,only,onto,opposite,or,other,others,otherwise,ought,our,ours,ourselves,out,outside,over,overall,own,particular,particularly,past,per,perhaps,placed,please,plus,possible,presumably,probably,provided,provides,que,quite,qv,rather,rd,re,'re,really,reasonably,recent,recently,regarding,regardless,regards,relatively,respectively,right,round,'s,said,same,saw,say,saying,says,second,secondly,see,seeing,seem,seemed,seeming,seems,seen,self,selves,sensible,sent,serious,seriously,seven,several,sha,shall,she,should,since,six,so,some,somebody,someday,somehow,someone,something,sometime,sometimes,somewhat,somewhere,soon,sorry,specified,specify,specifying,still,sub,such,sup,sure,t,take,taken,taking,tell,tends,th,than,thank,thanks,thanx,that,thats,the,their,theirs,them,themselves,then,thence,there,thereafter,thereby,therefore,therein,theres,thereupon,these,they,thing,things,think,third,thirty,this,thorough,thoroughly,those,though,three,through,throughout,thru,thus,till,to,together,too,took,toward,towards,tried,tries,truly,try,trying,twice,two,un,under,underneath,undoing,unfortunately,unless,unlike,unlikely,until,unto,up,upon,upwards,us,use,used,useful,uses,using,usually,v,value,various,'ve,versus,very,via,viz,vs,want,wants,was,way,we,welcome,well,went,were,what,whatever,when,whence,whenever,where,whereafter,whereas,whereby,wherein,whereupon,wherever,whether,which,whichever,while,whilst,whither,who,whoever,whole,whom,whomever,whose,why,will,willing,wish,with,within,without,wo,wonder,would,yes,yet,you,your,yours,yourself,yourselves,zero".split(",");
        // For testing also german and combined
        // German
//        String[] sws = "aber,als,am,an,auch,auf,aus,bei,bin,bis,bist,da,dadurch,daher,darum,das,daß,dass,dein,deine,dem,den,der,des,dessen,deshalb,die,dies,dieser,dieses,doch,dort,du,durch,ein,eine,einem,einen,einer,eines,er,es,euer,eure,für,hatte,hatten,hattest,hattet,hier,hinter,ich,ihr,ihre,im,in,ist,ja,jede,jedem,jeden,jeder,jedes,jener,jenes,jetzt,kann,kannst,können,könnt,machen,mein,meine,mit,muß,mußt,musst,müssen,müßt,nach,nachdem,nein,nicht,nun,oder,seid,sein,seine,sich,sie,sind,soll,sollen,sollst,sollt,sonst,soweit,sowie,und,unser,unsere,unter,vom,von,vor,wann,warum,was,weiter,weitere,wenn,wer,werde,werden,werdet,weshalb,wie,wieder,wieso,wir,wird,wirst,wo,woher,wohin,zu,zum,zur,über".split(",");
        // en + ger
//        String[] sws = "a,able,about,above,abroad,according,accordingly,across,actually,adj,after,afterwards,again,against,ago,ahead,ai,all,allow,allows,almost,alone,along,alongside,already,also,although,always,am,amid,amidst,among,amongst,an,and,another,any,anybody,anyhow,anyone,anything,anyway,anyways,anywhere,apart,appear,appreciate,appropriate,are,around,as,aside,ask,asking,associated,at,available,away,awfully,back,backward,backwards,be,became,because,become,becomes,becoming,been,before,beforehand,begin,behind,being,believe,below,beside,besides,best,better,between,beyond,both,brief,but,by,c'mon,ca,came,can,cannot,cant,caption,cause,causes,certain,certainly,changes,clearly,co,co.,com,come,comes,concerning,consequently,consider,considering,contain,containing,contains,corresponding,could,course,currently,'d,dare,definitely,described,despite,did,different,directly,do,does,doing,done,down,downwards,during,each,edu,eg,eight,eighty,either,else,elsewhere,end,ending,enough,entirely,especially,et,etc,even,ever,evermore,every,everybody,everyone,everything,everywhere,ex,exactly,example,except,fairly,far,farther,few,fewer,fifth,first,five,followed,following,follows,for,forever,former,formerly,forth,forward,found,four,from,further,furthermore,get,gets,getting,given,gives,go,goes,going,gone,got,gotten,greetings,had,half,happens,hardly,has,have,having,he,hello,help,hence,her,here,hereafter,hereby,herein,hereupon,hers,herself,hi,him,himself,his,hither,hopefully,how,howbeit,however,hundred,i,ie,if,ignored,immediate,in,inasmuch,inc,inc.,indeed,indicate,indicated,indicates,inner,inside,insofar,instead,into,inward,is,it,its,itself,just,k,keep,keeps,kept,know,known,knows,last,lately,later,latter,latterly,least,less,lest,let,like,liked,likely,likewise,little,'ll,look,looking,looks,low,lower,ltd,'m,made,mainly,make,makes,many,may,maybe,me,mean,meantime,meanwhile,merely,might,mine,minus,miss,more,moreover,most,mostly,mr,mrs,much,must,my,myself,name,namely,nd,near,nearly,necessary,need,needs,neither,never,neverf,neverless,nevertheless,new,next,nine,ninety,no,nobody,non,none,nonetheless,noone,no-one,nor,normally,not,nothing,notwithstanding,novel,now,nowhere,n't,obviously,of,off,often,oh,ok,okay,old,on,once,one,ones,only,onto,opposite,or,other,others,otherwise,ought,our,ours,ourselves,out,outside,over,overall,own,particular,particularly,past,per,perhaps,placed,please,plus,possible,presumably,probably,provided,provides,que,quite,qv,rather,rd,re,'re,really,reasonably,recent,recently,regarding,regardless,regards,relatively,respectively,right,round,'s,said,same,saw,say,saying,says,second,secondly,see,seeing,seem,seemed,seeming,seems,seen,self,selves,sensible,sent,serious,seriously,seven,several,sha,shall,she,should,since,six,so,some,somebody,someday,somehow,someone,something,sometime,sometimes,somewhat,somewhere,soon,sorry,specified,specify,specifying,still,sub,such,sup,sure,t,take,taken,taking,tell,tends,th,than,thank,thanks,thanx,that,thats,the,their,theirs,them,themselves,then,thence,there,thereafter,thereby,therefore,therein,theres,thereupon,these,they,thing,things,think,third,thirty,this,thorough,thoroughly,those,though,three,through,throughout,thru,thus,till,to,together,too,took,toward,towards,tried,tries,truly,try,trying,twice,two,un,under,underneath,undoing,unfortunately,unless,unlike,unlikely,until,unto,up,upon,upwards,us,use,used,useful,uses,using,usually,v,value,various,'ve,versus,very,via,viz,vs,want,wants,was,way,we,welcome,well,went,were,what,whatever,when,whence,whenever,where,whereafter,whereas,whereby,wherein,whereupon,wherever,whether,which,whichever,while,whilst,whither,who,whoever,whole,whom,whomever,whose,why,will,willing,wish,with,within,without,wo,wonder,would,yes,yet,you,your,yours,yourself,yourselves,zero,aber,als,am,an,auch,auf,aus,bei,bin,bis,bist,da,dadurch,daher,darum,das,daß,dass,dein,deine,dem,den,der,des,dessen,deshalb,die,dies,dieser,dieses,doch,dort,du,durch,ein,eine,einem,einen,einer,eines,er,es,euer,eure,für,hatte,hatten,hattest,hattet,hier,hinter,ich,ihr,ihre,im,in,ist,ja,jede,jedem,jeden,jeder,jedes,jener,jenes,jetzt,kann,kannst,können,könnt,machen,mein,meine,mit,muß,mußt,musst,müssen,müßt,nach,nachdem,nein,nicht,nun,oder,seid,sein,seine,sich,sie,sind,soll,sollen,sollst,sollt,sonst,soweit,sowie,und,unser,unsere,unter,vom,von,vor,wann,warum,was,weiter,weitere,wenn,wer,werde,werden,werdet,weshalb,wie,wieder,wieso,wir,wird,wirst,wo,woher,wohin,zu,zum,zur,über".split(",");
        for (String s : sws) {
            hs.add(s);
        }
        return hs;
    }
    private Boolean wordIsInStopword(String word) {
        return this.stopwordsEnLong.contains(word);
    }

    //******************************************************************************************************************
    //************************************* Cleanup stuff **************************************************************
    //******************************************************************************************************************
    public void deleteFile(String path) {
        // http://www.mkyong.com/java/how-to-delete-file-in-java/
        try {
            File file = new File(path);
            if (!file.delete()) {
                LOG.error("Delete operation failed.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //******************************************************************************************************************
    //************************************* word processing ************************************************************
    //******************************************************************************************************************
    public String stemEN(String token) {
        /**
         * Stemming, e.g. "running" becomes "run" aso.
         */
        stemmer.setCurrent(token);
        stemmer.stem();
        return stemmer.getCurrent();
    }

    public List<String> wordTokenize(String documentString) {
        /**
         * Tokenize a document into words. This is somewhat convoluted using Stanford NLP.
         */
        this.tkzr = PTBTokenizer.newPTBTokenizer(new StringReader(documentString));
        List<Word> toks = tkzr.tokenize();
        List<String> words = new ArrayList<String>(toks.size());
        for (Word tok : toks) {
            words.add(tok.toString().toLowerCase()); // all words in lower case to reduce dimensions
        }
        return words;
    }

    //******************************************************************************************************************
    //************************************* raw word frequency info*****************************************************
    //******************************************************************************************************************
    public HashMap<String, Integer> rawTF(EdpDocument doc, TEXT_FIELD option) {
        /**
         * Get map of terms (string) and their counts (Integer) from a document.
         * @return a map of term -> count mappings. This makes sense as the final dot product used for sim(doc1,doc2)
         * only involves terms both documents share, i.e. only the intersection terms of the 2 maps will be needed for
         * calculation. The maps make it a sparse representation, while also making it possible introspect easily.
         */
        HashMap<String, Integer> rawTFs = new HashMap<String, Integer>();
        String text = null;
        if (option == TEXT_FIELD.Description) {
            text = doc.description;
        } else if (option== TEXT_FIELD.TITLE) {
            text = doc.title;
        } else {
            throw new NullPointerException("Invalid text content selector: use one from TEXT_FIELD");
        }
        for (String word : this.wordTokenize(text)) {
            // clean and stem each word
            String cleanedWord = this.wordOnly(word);
            if (!cleanedWord.isEmpty() && !wordIsInStopword(word)) { // no stopwords, no word-less tokens
                String cleanWord = this.stemEN(cleanedWord);
                this.updateTermCountByOne(cleanedWord, rawTFs);
            }
        }
        return rawTFs;
    }

    private void updateTermCountByOne(String term, HashMap<String, Integer> countMap) {
        /**
         * Updated a term count by one. Can be used for TF and DF alike.
         */
        if (countMap.containsKey(term)) {
            // increment raw TF by one
            countMap.put(term, countMap.get(term) + 1);
        } else {
            // add new term
            countMap.put(term, 1);
        }
    }

    public void updateDFs(HashMap<String, Integer> TFs, HashMap<String, Integer> DFs) {
        // Update DF counts by one for terms the current document contains.
        for (String term : TFs.keySet()) {
            updateTermCountByOne(term, DFs);
        }
    }

    public void genTFbuildCorpus(String objectsStoragePath) throws IOException {
        /**
         * main data generation step
         * Collect TF (term freq. per document) and DF (term freq per corpus (0 or 1 per document)) info
         */
        // raw document frequency, if a term t \in document d, then TF_t count goes up by one (only once per document)
        // counts if a term is present in a document or not
        // corpus wide number of documents
        int numDocs = 0;
        HashMap<String, Integer> rawDescriptionDFs = new HashMap<String, Integer>();
        HashMap<String, Integer> rawTitleDFs       = new HashMap<String, Integer>();

        // corpus assembly, from sparql queries
        FileOutputStream fos    = null;
        SparqlIterator docIter  = new SparqlIterator(triplestoreUrl, triplestoreGraph);
        try {
            fos = new FileOutputStream(objectsStoragePath);
            for (int i = 0; i != maxDocs; i++) {
                if (i%100 == 0)
                    System.out.println("\t#" + i);
                EdpDocument doc = docIter.nextDoc();
                if (doc == null) {
                    System.out.println("end of db");
                    break; // end of DB
                }
                numDocs += 1;
                // count TFs in document
                HashMap<String, Integer> rawDescriptionTFs = rawTF(doc, TEXT_FIELD.Description);
                HashMap<String, Integer> rawTitleTFs = rawTF(doc, TEXT_FIELD.TITLE);
                String[] id = doc.docID.split("/");
                // Assemble a discrete word distribution storage object for this document
                EDPPickleTF edpTF = new EDPPickleTF( id[id.length - 1],
                                                 rawTitleTFs,
                                                 rawDescriptionTFs,
                                                 doc.tags,
                                                 doc.categories);

                storeObjAsLine(fos, edpTF);
                // count DF in corpus using document TF term (suing keys)
                updateDFs(rawDescriptionTFs, rawDescriptionDFs);
                updateDFs(rawTitleTFs, rawTitleDFs);
//                ////// debug
//                System.out.println(rawTFs);
            }
        } finally {
            if (fos != null)
                fos.flush();
                fos.close();
        }
        // keep document frequency info as an object, its smallish and always needed
        this.rawDescriptionDFs = rawDescriptionDFs;
        this.rawTitleDFs = rawTitleDFs;
        this.numDocs = numDocs;

//        //// debug
//        System.out.println(this.rawDFs);
    }

    /**
     * Takes a TF corpus and processes it into an TFIDF corpus. This makes sense since TFIDF will be needed multiple
     * times.
     *
     * @param objectsStoragePath is the path to the corpus of raw term frequencies.
     * @return path to TFIDFnt and TFIDFat corpus
     */
    public String[] transIntoTFIDFCorpus(String objectsStoragePath) throws IOException, ClassNotFoundException {
        // Check if storing to list of objects worked.
        FileInputStream fis = null;
        FileOutputStream fosnt = null;
        FileOutputStream fosat = null;
        String tfidfPathnt = objectsStoragePath + "ntTFIDF.tmp";
        String tfidfPathat = objectsStoragePath + "atTFIDF.tmp";
        try {
            fis = new FileInputStream(objectsStoragePath);
            fosnt = new FileOutputStream(tfidfPathnt);
            fosat = new FileOutputStream(tfidfPathat);
            while (true) {
                // load rawTF
                ObjectInputStream ois = new ObjectInputStream(fis);
                EDPPickleTF docTFs = (EDPPickleTF)ois.readObject();
                // Transform TF into TFIDF schemes using DFs
                // title at,nt types of TFIDF
                HashMap<String, Double> titleTFIDFnt = mapToTFIDFnt(docTFs.title, this.rawTitleDFs, this.numDocs);
                HashMap<String, Double> titleTFIDFat = mapToTFIDFat(docTFs.title, this.rawTitleDFs, this.numDocs);
                // description
                HashMap<String, Double> descTFIDFat = mapToTFIDFat(docTFs.description, this.rawDescriptionDFs, this.numDocs);
                HashMap<String, Double> descTFIDFnt = mapToTFIDFnt(docTFs.description, this.rawDescriptionDFs, this.numDocs);
                // assemble TFIDF objects
                EDPPickleTFIDF docTFIDFnt = new EDPPickleTFIDF(docTFs.docID, titleTFIDFnt, descTFIDFnt, docTFs.tags, docTFs.categories);
                EDPPickleTFIDF docTFIDFat = new EDPPickleTFIDF(docTFs.docID, titleTFIDFat, descTFIDFat, docTFs.tags, docTFs.categories);
                // store them
                storeObjAsLine(fosnt, docTFIDFnt);
                storeObjAsLine(fosat, docTFIDFat);
            }
        } catch (EOFException ignored) {
            // as expected
        } finally {
            if (fis != null)
                fis.close();
            if (fosnt != null) {
                fosnt.close();
            }
            if (fosat != null) {
                fosat.close();
            }
        }
        String[] paths = {tfidfPathnt, tfidfPathat};
        // return paths to both TFIDF variants
        return paths;
    }

    private void storeObjAsLine(FileOutputStream fos, Object o) throws IOException {
        /**
         * Stores a new object as a line in a file. This file then contains a list of objects, making parsing faster and
         * avoiding the need for a parser.
         */
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(o);
    }

    private double IDFt(Integer nt, Integer numDocs) {
        /**
         * t version of IDF according to SMART notation: log #docs_in_corpus / ...
         * @see https://en.wikipedia.org/wiki/SMART_Information_Retrieval_System
         */
        if (nt == 0) {
            return 0;
        } else {
            return Math.log((double) numDocs / (double) nt);
        }
    }

    private double TFa(Integer rawTF, Integer maxTFinDoc) {
        /**
         * a version of TF according to SMART notation: 0.5 + (0.5 * rawTF) / ...
         * @see https://en.wikipedia.org/wiki/SMART_Information_Retrieval_System
         */
        double TFa = 0.5d;
        if (rawTF == 0 || maxTFinDoc == 0) {
            return 0.5d;
        } else {
            TFa += .5d * rawTF / maxTFinDoc;
            return TFa;
        }
    }

    //******************************************************************************************************************
    //************************************* Similarities ***************************************************************
    //******************************************************************************************************************
    public void computeSimilarities(String[] tfidfPaths, double threshold) throws IOException, ClassNotFoundException {
        /**
         * Main computation step. Does an N x N comparison between document information(s) to find similar documents.
         * according to SMART scheme:
         * query at, corpus document nt
         */
        // Check if storing to list of objects worked.
        JSONGenerator jsonGen= JsonGeneratorFactory.getInstance().newJsonGenerator();
        FileInputStream queryDocs = null; // binary list of serialized tfidf documents in nt SMART format
        Writer outDescr = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile+"_description.sims"), "UTF-8"));
        Writer outtitle = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile+"_title.sims"), "UTF-8"));
        Writer outCateg = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile+"_category.sims"), "UTF-8"));
        Writer outTags = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile+"_tag.sims"), "UTF-8"));
        try {
            queryDocs = new FileInputStream(tfidfPaths[1]);
            FileInputStream corpusDocs = null; // binary list of serialized tfidf documents in at SMART format
            while (true) {
                // get query TFIDF
//                HashMap<String, Double> tfidfQuery = readTFIDFfromObjectStream(queryDocs);
                EDPPickleTFIDF tfidfQuery = (EDPPickleTFIDF)readObjectStream(queryDocs);
                // compare query tfidf to all following document tfidfs (documents preceeding the query have already been compared)
                try {
                    corpusDocs = new FileInputStream(tfidfPaths[0]);
                    corpusDocs.skip(queryDocs.getChannel().position()); // skip documents that precede the query
                    while (true) {
                        EDPPickleTFIDF tfidfDoc = (EDPPickleTFIDF)readObjectStream(corpusDocs);
                        // DESCRITPION SIM
                        // Finally this is what we want to do!
                        double descrSim = cosineSim(tfidfQuery.description, tfidfDoc.description);
                        if (descrSim > descriptionThreshold) {
                            outDescr.write(createJSONline(tfidfQuery.docID, tfidfDoc.docID, DUPLICATE_DESCRIPTION, descrSim, jsonGen));
                            outDescr.write('\n');
                        }
                        // TITLE SIM
                        double titleSim = cosineSim(tfidfQuery.title, tfidfDoc.title);
                        if (titleSim > titleThreshold) {
                            outtitle.write(createJSONline(tfidfQuery.docID, tfidfDoc.docID, DUPLICATE_TITLE, titleSim, jsonGen));
                            outtitle.write('\n');
                        }
                        // CATEGORY SIM
                        double categorySim = jaccardSim(tfidfQuery.categories, tfidfDoc.categories);
                        if (categorySim > categoryThreshold) {
                            outCateg.write(createJSONline(tfidfQuery.docID, tfidfDoc.docID, DUPLICATE_CATEGORIES, categorySim, jsonGen));
                            outCateg.write('\n');
                        }
                        // TAGS SIM
                        double tagSim = jaccardSim(tfidfQuery.tags, tfidfDoc.tags);
                        if (tagSim > tagThreshold) {
                            outTags.write(createJSONline(tfidfQuery.docID, tfidfDoc.docID, DUPLICATE_TAGS, tagSim, jsonGen));
                            outTags.write('\n');
                        }
                    }
                } catch (EOFException ignored) {
                    // as expected, because this has no next() method. EOF throws an Exception, so we catch it
                } finally {
                    if (corpusDocs != null)
                        corpusDocs.close();
                }
            }
        } catch (EOFException ignored) {
            // as expected, catching this once implicitly means the other file is EOF as well, which is nice
        } finally {
            if (queryDocs != null)
                queryDocs.close();
        }
        outDescr.flush();
        outtitle.flush();
        outCateg.flush();
        outTags.flush();
        outDescr.close();
        outtitle.close();
        outCateg.close();
        outTags.close();
    }

    public static String createJSONline(String originalDatasetID, String duplicateDatasetID, String field, double similarity, JSONGenerator generator){
        /**
         * Transform duplicates to JSON.
         */
        Map data = new HashMap();
        // String[0] is URL, its unused
        // Original [datasetID, distributionID]
        data.put(ORIGINAL_DATASET, originalDatasetID);
        data.put(field, duplicateDatasetID);
        data.put(SIMILARITY, similarity);

        // serialize to JSON
        return generator.generateJson(data);
    }

    private HashMap<String, Double> readTFIDFfromObjectStream(FileInputStream queryDoc) throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(queryDoc);
        return (HashMap<String, Double>) ois.readObject();
    }

    private Object readObjectStream(FileInputStream queryDoc) throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(queryDoc);
        return ois.readObject();
    }

    private String readDocIDFromStream(FileInputStream IDsCorpus) throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(IDsCorpus);
        return (String) ois.readObject();
    }

    private double dotProduct(ArrayList<Double> TFIDF1, ArrayList<Double> TFIDF2) {
        /**
         * This expects that all elements are in TFIDF 1 and 2 are at the same positions, i.e. of the same string token.
         * Elements that are 0.0d are not expected, as previous methods should determine sparsity.
         */
        assert TFIDF1.size() == TFIDF2.size() : "Both vectors are assumed to have the same length";
        double sum = 0.0d;
        for (int i = 0; i != TFIDF1.size(); i++) {
            sum += TFIDF1.get(i) * TFIDF2.get(i);
        }
        return sum;
    }

    private double euclidNorm(Collection<Double> TFIDS) {
        double len = 0.0d;
        for (double TFIDF : TFIDS) {
            len += Math.pow(TFIDF, 2);
        }
        return Math.sqrt(len);
    }

    private double cosineSim(HashMap<String, Double> tfidfQuery, HashMap<String, Double> tfidfDoc) {
        /**
         * c version of normalization according to SMART.
         */

        Pair<ArrayList<Double>, ArrayList<Double>> vec1and2 = getMatchingTFIDFs(tfidfQuery, tfidfDoc);
        double dot = dotProduct(vec1and2.getLeft(), vec1and2.getRight());
        if (dot > 0.0d) {
            return dot / (euclidNorm(tfidfQuery.values()) * euclidNorm(tfidfDoc.values()));
        } else {
            return 0.0d;
        }
    }

    private double jaccardSim(HashSet<String> A, HashSet<String> B) {
        HashSet<String> shortSet = null;
        HashSet<String> longSet  = null;

        if (A.size() <= B.size()) {
            shortSet = A;
            longSet  = B;
        } else {
            shortSet = B;
            longSet  = A;
        }
        int matches = 0;
        for(String e : shortSet) {
            if (longSet.contains(e)) {
                matches += 1;
            }
        }
        if (matches == 0) {
            return 0.0d;
        } else {
            int unionSize = A.size() + B.size() - matches;
            if (USE_JACCARD_ONLY_ON_LARGER_SETS && unionSize < 3) {
                // Ignore small sets (only 1-2 elements), as they are often wrong or default data. Not informative
                return 0.0d;
            }
            return ((double) matches / (double) unionSize); // only count matches once not 2x
        }
    }

    private Pair<ArrayList<Double>, ArrayList<Double>> assembleTFIDFat(HashMap<String, Integer> query,
                                                                       HashMap<String, Integer> doc2,
                                                                       HashMap<String, Integer> DF,
                                                                       Integer numDocs) {
        /**
         * Assemble TFIDF weights for a (query, corpus document) pair.
         * @param query is query with       at weighting (@see https://en.wikipedia.org/wiki/SMART_Information_Retrieval_System)
         * @param doc2 corpus document with nt weighting (@see https://en.wikipedia.org/wiki/SMART_Information_Retrieval_System)
         */
        ArrayList<Double> vec1 = new ArrayList<Double>();
        ArrayList<Double> vec2 = new ArrayList<Double>();
        // needed for augmented (a) term frequency
        Integer max1Freq = Collections.max(query.values());
        HashSet<String> matches = filterForMatchingTerms(query, doc2);
        for (String term : matches) { // This is not extremely efficient but makes the code easier to understand
            // get IDF
            double idft = IDFt(DF.get(term), numDocs);
            // multiply augmented TF to IDF
            vec1.add(TFa(query.get(term), max1Freq) * idft);
            vec2.add((double) doc2.get(term) * idft);// make query at scheme
        }
        return new ImmutablePair<ArrayList<Double>, ArrayList<Double>>(vec1, vec2);
    }

    private HashMap<String, Double> mapToTFIDFnt(HashMap<String, Integer> doc,
                                                 HashMap<String, Integer> DF,
                                                 Integer numDocs) {
        /**
         * nt TFIDF weighting for a corpus document.
         * This weighting scheme is used for the document (i.e. not the query)
         */
        HashMap<String, Double> TFIDFmap = new HashMap<String, Double>();
        for (String term : doc.keySet()) {
            // get IDF
            double idft = IDFt(DF.get(term), numDocs);
            TFIDFmap.put(term, (double) doc.get(term) * idft);
        }
        return TFIDFmap;
    }

    private HashMap<String, Double> mapToTFIDFat(HashMap<String, Integer> doc,
                                                 HashMap<String, Integer> DF,
                                                 Integer numDocs) {
        /**
         * nt TFIDF weighting for a corpus document.
         * This weighting scheme is used for the document (i.e. not the query)
         */
        HashMap<String, Double> TFIDFmap = new HashMap<String, Double>();
        if (doc.size()>0) { // sometimes the field is empty
            Integer maxFreq = Collections.max(doc.values());
            for (String term : doc.keySet()) {
                // get IDF
                double idft = IDFt(DF.get(term), numDocs);
                TFIDFmap.put(term, TFa(doc.get(term), maxFreq) * idft);
            }
        }
        return TFIDFmap;
    }

    private HashSet<String> filterForMatchingTerms(HashMap<String, Integer> doc1, HashMap<String, Integer> doc2) {
        /**
         * Given two term-frequency-per-term maps this returns 2 double vectors of TermFrequencies for matching.
         * TODO: this is not optimal regaring performance, but more readable.
         */
        HashSet<String> matches = new HashSet<String>();
        // only use the shortest map to scan for matches
        if (doc1.size() < doc2.size()) {
            for (String term : doc1.keySet()) {
                if (doc2.containsKey(term)) {
                    matches.add(term);
                }
            }
        } else { // redundant but less computations + very readable
            for (String term : doc2.keySet()) {
                if (doc1.containsKey(term)) {
                    matches.add(term);
                }
            }
        }
        return matches;
    }

    private Pair<ArrayList<Double>, ArrayList<Double>> getMatchingTFIDFs(HashMap<String, Double> doc1, HashMap<String, Double> doc2) {
        ArrayList<Double> TFIDFs1 = new ArrayList<Double>();
        ArrayList<Double> TFIDFs2 = new ArrayList<Double>();
        // Temporaries (only pointers), to optimize computation
        HashMap<String, Double> shorter = null;
        HashMap<String, Double> longer = null;
        if (doc1.size() < doc2.size()) { // TODO if order would matter we could simply save this value and use it for return value switch
            shorter = doc1;
            longer = doc2;
        } else {
            shorter = doc2;
            longer = doc1;
        }
        // only assemble TFIDF for matches
        for (String term : shorter.keySet()) {
            if (longer.containsKey(term)) {
                TFIDFs1.add(shorter.get(term));
                TFIDFs2.add(longer.get(term));
            }
        }
        return new ImmutablePair<ArrayList<Double>, ArrayList<Double>>(TFIDFs1, TFIDFs2);
    }
}
