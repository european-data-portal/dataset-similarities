package de.fhg.fokus.paneodp.similarities;

import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;

import java.util.HashSet;
import java.util.NoSuchElementException;

/**
 * Created by Nils Rethmeier on 10.09.15.
 */
public class SparqlIterator {
    /**
     * Class that iterates over a sparql triplestoreGraph and returns EDP project documents.
     * This is an abstraction of streamed, batch db requests/queries.
     */
    // setup
    private String triplestoreUrl;
    private String triplestoreGraph;
    private int chunkSize = 8000;         // SPARQL LIMIT, or here: the line cache
    // states
    private int iteratorPosition = 0;     // SPARQL OFFSET for batch db queries
    private ResultSet results;
    private String currentDocID = "";
    private QuerySolution prevQuerySolution = null;
    private Boolean start = true;

    public SparqlIterator(String triplestoreUrl, String triplestoreGraph, int chunkSize) {
        /**
         * @param chunkSize is a caching value. 1000 is decently fast.
         */
        this.chunkSize = chunkSize;
        this.triplestoreUrl = triplestoreUrl;
        this.triplestoreGraph = triplestoreGraph;
    }

    public SparqlIterator(String triplestoreUrl, String triplestoreGraph) {
        this(triplestoreUrl, triplestoreGraph, 8000);
    }

    public EdpDocument nextDoc() {
        /**
         * Retrieves the next document form a database. This encapsulates streamed, batch processing done on the data base.
         * End of DB is reached if NULL is returned.
         */
        String docID = "";
        String title = "";
        String description = "";
        HashSet<String> tags = new HashSet<String>();
        HashSet<String> categories = new HashSet<String>();
        String URLtail = "";
        QuerySolution n = null;

        while (true) {
            n = this.next(); // basically look one step into the future, to see when the current document ends
            if (n==null && this.prevQuerySolution == null ) {
                // last document emitted
                return null;
            }
            else if (n == null) { // special case for end of database (last document)
//                System.out.println("if 1");
                // add info form previous next
                docID = this.prevQuerySolution.get("DocID").toString();
                title = this.prevQuerySolution.get("docTitle").toString();
                description = this.prevQuerySolution.get("doctext").toString();
                tags.add(this.prevQuerySolution.get("docTags").toString());
                categories.add(this.prevQuerySolution.get("categories").toString());
                // take note that end of DB is reached
                this.prevQuerySolution = n;
                // generate last document from database
                return new EdpDocument(docID, title, description, tags, categories, URLtail);
            } else if (n.get("DocID").toString().equals(this.currentDocID)) {
//                System.out.println("elif 1");
                // continue collecting info on current document
                docID = this.prevQuerySolution.get("DocID").toString();
                title = this.prevQuerySolution.get("docTitle").toString();
                description = this.prevQuerySolution.get("doctext").toString();
                tags.add(this.prevQuerySolution.get("docTags").toString());
                categories.add(this.prevQuerySolution.get("categories").toString());
                // update by one property step
                this.prevQuerySolution = n;
            } else { // one new document generated, since doc ID changed
//                System.out.println("else 1");
                // yield/ emmit a new document
                if (this.start) {
//                    System.out.println("if 2");
                    this.start = false; // special case for the first document

                    // update by one step, prepare for new document when nextDoc is called again
                    this.currentDocID = n.get("DocID").toString();
                    this.prevQuerySolution = n;
                } else {
                    // finish collecting info on the previous document, otherwise we would miss a line/property
                    docID = this.prevQuerySolution.get("DocID").toString();
                    title = this.prevQuerySolution.get("docTitle").toString();
                    description = this.prevQuerySolution.get("doctext").toString();
                    tags.add(this.prevQuerySolution.get("docTags").toString());
                    categories.add(this.prevQuerySolution.get("categories").toString());

                    // update by one step, prepare for new document when nextDoc is called again
                    this.currentDocID = n.get("DocID").toString();
                    this.prevQuerySolution = n;

                    return new EdpDocument(docID, title, description, tags, categories, URLtail);
                }
            }
        }
    }

    private QuerySolution next() {
        /**
         * make batch iteration feel like full database iteration. Returns null iif end of database is reached
         * @return the next line of db results, or null if last document form data base was returned in the step before.
         */
        if (this.start) { // this is a bit ugly
            advanceBatch();
        }
        if (this.results.hasNext()) {
            return this.results.next();
        } else { // make batch iteration feel like full database iteration
            advanceBatch();
            try {
                QuerySolution n = this.results.next();
                return n;
            } catch (NoSuchElementException nse) {
                return null;
            }
        }
    }

    private void advanceBatch() {
        QueryExecution x = QueryExecutionFactory.sparqlService(triplestoreUrl, String.format(updateQuery(), triplestoreGraph));
        ResultSet resultz = x.execSelect();
        resultz.hasNext(); // this fails if end of database is reached
        this.results = resultz;
    }

    // TODO check if the same docID is encountered later during processing, if so, this is inconsistent

    private String updateQuery() {
        /**
         * Create a new sparql query, for batch stream processing.
         * @return the query string.
         */
        String endpointSparql = // TODO optimize this, but its may not be that necessary
                        "BASE <http://publicdata.eu/set/>\n" +
                        "PREFIX tags: <http://www.w3.org/ns/dcat#>\n" +
                        "PREFIX docTitle: <http://purl.org/dc/terms/>\n" +
                        "PREFIX doc: <http://publicdata.eu/set/>\n" +
                        "PREFIX docText: <http://purl.org/dc/terms/>\n" +
                        "PREFIX distributions: <http://www.w3.org/ns/dcat#>\n" +
                        "PREFIX categories: <http://www.w3.org/ns/dcat#>" +
                        "select ?DocID ?docTitle ?doctext ?docTags ?attachedDocs ?categories where {\n" +
                        "?DocID ?p <http://www.w3.org/ns/dcat#Dataset>.\n" +
                        "?DocID tags:keyword ?docTags.\n" +
                        "?DocID docTitle:title ?docTitle.\n" +
                        "?DocID docText:description ?doctext.\n" +
                        "?DocID distributions:distribution ?attachedDocs.\n" +
                        "?DocID categories:theme ?categories." +
                        "} offset " + Integer.toString(this.iteratorPosition) + // TODO used to have an orderby directive (10k lines limit), does this still work?
                        " limit " + Integer.toString(this.chunkSize);
        this.iteratorPosition += chunkSize; // prepare for calling next time, i.e. move one batch further
        return endpointSparql;
    }


}
